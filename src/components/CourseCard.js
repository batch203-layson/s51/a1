import { useState } from "react";

import { Card, Button } from 'react-bootstrap';
// destructure the "courseProp" from the prop parameter
// CourseCard(prop)
export default function CourseCard({ courseProp }) {

    // console.log(props.courseProp.name);
    // console.log(typeof props);
    // console.log(courseProp);

    // Scenario: Keep track the number of enrollees of each course.

    // Destructure the course properties into their own variables
    const { name, description, price } = courseProp;

    // Syntax:
    // const [stateName, setStateName] = useState(initialStateValue);
    // Using the state hook, it returns an array with the following elements:
    // first element contains the the current inital State value.
    // second element is a function that is used to change the value of the first element.

    const [count, setCount] = useState(0);
    const [seat, setSeat] = useState(30)
    //console.log(useState(10));

    function enroll() {
        // setSeat(seat - 1);
        // console.log(Seats: ${ seat })
        if (seat===0&&count===30)
        {
            alert("No more seats!!!!!");
        }
        else
        {
            setSeat(seat - 1);
            setCount(count + 1);
            console.log(`Enrollees: ${seat}`);
        }
        
        
    }

    return (
        <Card>
            <Card.Body>
                <Card.Title>
                    {name}
                </Card.Title>
                <Card.Subtitle>
                    Description:
                </Card.Subtitle>
                <Card.Text>
                    {description}
                </Card.Text>
                <Card.Subtitle>
                    Price:
                </Card.Subtitle>
                <Card.Text>
                    Php {price}
                </Card.Text>
                <Card.Text>
                    {count} enrollees
                </Card.Text>
                <Button variant="primary" onClick={enroll}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}
